import React from "react";

export const TodoItem = ({ todo, onDelete }) => {
  let style = {
    width: "18rem",
  };
  return (
    <>
      <div className="card" style={style}>
        <div className="card-body">
          <h5 className="card-title">{todo.title}</h5>
          <p className="card-text">{todo.desc}</p>
          <button
            className="btn btn-danger"
            onClick={() => {
              onDelete(todo);
            }}
          >
            Delete
          </button>
        </div>
      </div>

      <br />
    </>
  );
};
